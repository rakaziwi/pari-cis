<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {

    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::group(['middleware' => ['isAdmin']], function() {
        Route::resource('event', 'EventController', ['except' => ['destroy']]);
        Route::get('event/{event}/delete', 'EventController@destroy')->name('event.delete');

        Route::name('workshop.')->prefix('workshop')->group(function() {
            Route::get('create/{event}', 'WorkshopController@create')->name('create');
            Route::get('{workshop}/delete', 'WorkshopController@destroy')->name('delete');
            Route::get('schedule/{filterDate?}', 'WorkshopController@schedule')->name('schedule');
        });
        Route::resource('workshop', 'WorkshopController', ['except' => ['create', 'destroy']]);

        Route::name('session.')->prefix('session')->group(function() {
            Route::get('create/{workshop}', 'WorkshopSessionController@create')->name('create');
            Route::get('{workshop}/delete', 'WorkshopSessionController@destroy')->name('delete');
        });
        Route::resource('session', 'WorkshopSessionController', ['except' => ['create', 'destroy']]);

        Route::name('participant.')->prefix('participant')->group(function() {
            Route::get('{participant}/delete', 'ParticipantController@destroy')->name('delete');
            Route::get('create/{workshop}', 'ParticipantController@createInWorkshop')->name('createInWorkshop');
            Route::post('store/{workshop}', 'ParticipantController@storeInWorkshop')->name('storeInWorkshop');
            Route::get('show/{workshop}', 'ParticipantController@show')->name('showAvailable');
        });
        Route::resource('participant', 'ParticipantController', ['except' => ['destroy']]);
    });

    Route::group(['middleware' => ['isSuper']], function() {
        Route::prefix('user-management')->namespace('UserManagement')->group(function() {
            Route::resource('user', 'UserController', ['except' => ['destroy']]);
            Route::get('user/{user}/delete', 'UserController@destroy')->name('user.delete');
        });
    });
    
    Route::name('attendance.')->prefix('attendance')->group(function() {
        Route::get('/', 'ParticipantAttendanceController@index')->name('index');
        Route::get('{event}/workshop', 'ParticipantAttendanceController@workshop')->name('workshop');
        Route::get('{workshopSession}/scan', 'ParticipantAttendanceController@scan')->name('scan');
        Route::get('{workshopSession}/report', 'ParticipantAttendanceController@report')->name('report');
        Route::get('{event}/overallEventReport', 'EventController@overallReport')->name('overallEventReport');
        Route::get('{event}/exportOverallEventReport', 'EventController@exportOverallReport')->name('exportOverallEventReport');
        Route::get('{workshop}/overallReport', 'ParticipantAttendanceController@overallReport')->name('overallReport');
        Route::get('{workshop}/exportOverallReport', 'ParticipantAttendanceController@exportOverallReport')->name('exportOverallReport');
        Route::post('register', 'ParticipantAttendanceController@register')->name('register');
        Route::get('{attendance}/delete', 'ParticipantAttendanceController@destroy')->name('delete');
        Route::get('{event}/redirect', 'ParticipantAttendanceController@redirectToWorkshop')->name('redirect');
    });

    Route::prefix('api/v0')->namespace('Apis\v0')->group(function() {
        Route::get('/scan', 'ParticipantAttendanceController@scanBarcode')->name('scan');
        Route::get('/updateTime/{workshopId}/{participantId}', 'ParticipantAttendanceController@updateAttendanceParticipantTime')->name('updateTime');
    });

});
