@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary mb-4">
    <div class="panel-heading">{{ $workshop->judul_materi }} - {{ $session->sesi }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2">Speaker</div>
            <div class="col-sm-10 text-muted">{{ $workshop->pengisi_materi }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Place/Room</div>
            <div class="col-sm-10 text-muted">{{ $workshop->tempat }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Description</div>
            <div class="col-sm-10 text-muted">{{ $workshop->deskripsi }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Session</div>
            <div class="col-sm-10 text-muted">{{ $session->sesi }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Session Time</div>
            <div class="col-sm-10 text-muted">{{ $session->waktu_mulai->format('H:i') }} to {{ $session->waktu_akhir->format('H:i') }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Duration</div>
            <div class="col-sm-10 text-muted">{{ $session->durasi }} Minutes</div>
        </div>
        <div class="text-right">
            <a href="{{ route('attendance.report', ['sessionId' => $session->id]) }}" class="btn btn-info btn-sm text-white text-sm-center mt-2">
                <i class="fa fa-book"></i> Attendance Report
            </a>
            <a href="{{ route('attendance.workshop', ['eventId' => $workshop->event_id]) }}" class="btn btn-danger btn-sm text-white text-sm-center mt-2">
                <i class="fa fa-arrow-circle-o-left"></i> Back
            </a>
        </div>
    </div>
</div>

{!! Form::open(['id' => 'scan-form']) !!}

<div class="input-group">
    <input type="text" class="form-control" id="barcode" placeholder="Scan or Input The Participant Number..." autofocus>
    <div class="input-group-btn">
        <button class="btn btn-primary" type="submit"><span class="fa fa-barcode"></span> Scan</button>
    </div>
</div>

{!! Form::close() !!}

<div id="alert-scan" class="alert alert-success mt-4" role="alert" style="display:none">
    <label id="alert-label">molohok</label>
</div>

<script>

$(document).ready(function() {

start = moment('{{ $session->waktu_mulai }}').subtract({ hours:1 });
end = '{{ $session->waktu_akhir }}';
if (!moment().isBetween(start, end)) {
    urlRedirect = "{{ route('attendance.redirect', ['eventId' => $workshop->event_id]) }}?sesi={{ $session->sesi }}&judul_materi={{ $workshop->judul_materi }}";
    // window.location = urlRedirect;
}

$('#barcode').focusout(function(){
    $('#barcode').focus();
});

$('#scan-form').submit(function(e) {
    e.preventDefault();
    barcode = $('#barcode').val();
    scanTime = moment().format('YYYY-MM-DD HH:mm');

    $.ajax({
        type: 'GET',
        url: "{{ route('scan') }}",
        data: {
            workshop_id: {{ $workshop->id }},
            session_id: {{ $session->id }},
            barcode: barcode,
            scan_time: scanTime
        },
        beforeSend: function() {
            $('#alert-scan').hide();
        },
        success: function(result) {
            $('#alert-scan').removeClass('alert-danger');
            $('#alert-scan').removeClass('alert-success');
            if (result.success != false) {
                $('#alert-scan').addClass('alert-success');
            } else {
                $('#alert-scan').addClass('alert-danger');
            }
            $('#alert-label').text(result.message);
            $('#alert-scan').slideDown(300);
            $('#alert-scan').delay(3000).slideUp(300);
        },
        complete: function() {
            $('#barcode').val('').focus();
        },
        error: function(result, status, xhr) {
            if (result.status == 404) {
                message = result.responseJSON.message;
                urlRedirect = "{{ route('attendance.redirect', ['eventId' => $workshop->event_id]) }}?sesi={{ $session->sesi }}&judul_materi={{ $workshop->judul_materi }}";
                window.location = urlRedirect;
            } else {
                $('#alert-scan').removeClass('alert-danger');
                $('#alert-scan').removeClass('alert-success');
                $('#alert-scan').addClass('alert-danger');
                $('#alert-label').text(result.responseJSON.message);
                $('#alert-scan').slideDown(300);
                $('#alert-scan').delay(3000).slideUp(300);
            }
        }
    });
});

});

</script>

@stop

@section('libs')
  <script type="text/javascript" src="{{ asset('datetime-picker/moment.min.js') }}"></script>
@stop