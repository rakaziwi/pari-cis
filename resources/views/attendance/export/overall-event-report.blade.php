<html>

<table>
    <tr>
        <td>Event</td>
        <td>{{ $event->judul_materi }}</td>
    </tr>
    <tr>
        <td>Place</td>
        <td>{{ $event->tempat }}</td>
    </tr>
    <tr>
        <td>Time</td>
        <td>{{ $event->tanggal_mulai }} to {{ $event->tanggal_akhir }}</td>
    </tr>
    <tr>
        <td>Duration</td>
        <td>{{ $event->durasi }} minutes</td>
    </tr>
</table>

<table>
    <thead>
        <tr>
            <th>Participant Number</th>
            <th>Name</th>
            <th>Identity Number</th>
            <th>KTA Number</th>
            <th>Institute</th>
            <th>Total Time Attendance (Minutes)</th>
            <th>Attendance Percentage (%)</th>
            <th>Pass Status</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($participants as $participant)
            <tr>
                <td>{{ $participant->kode_peserta }}</td>
                <td>{{ $participant->nama }}</td>
                <td>{{ $participant->no_ktp }}</td>
                <td>{{ $participant->no_kta }}</td>
                <td>{{ $participant->institusi }}</td>
                <td>{{ $participant->waktu_kehadiran }}</td>
                <td>{{ $participant->persentase_kehadiran }}</td>
                <td>@if($participant->persentase_kehadiran >= 75) Passed @else Not Passed @endIf</td>
            </tr>
        @empty
            <tr>
                <td colspan="8" class="text-center text-muted">No data</td>
            </tr>
        @endforelse
    </tbody>
</table>

</html>