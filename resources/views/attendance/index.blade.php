@extends('layouts.app-base')

@section('content')

<div class="row mb-2">
@forelse ($events as $event)
  <div class="col-sm-6">
    <div class="panel panel-primary">
      <div class="panel-body text-white bg-primary" style="height:150px; overflow-y:auto">
        <h3>{{$event->nama}}</h3>
        <p class="card-text">{{$event->keterangan}}</p>
      </div>
      <ul class="list-group">
        <li class="list-group-item" style="height:50px">
          <div class="row">
            <div class="col-md-1"><strong>Place</strong></div>
            <div class="col-md-1"><strong>:</strong></div>
            <div class="col-md-10">{{$event->tempat}}</div>
          </div>
        </li>
        <li class="list-group-item" style="height:50px">
          <div class="row">
            <div class="col-md-1"><strong>Date</strong></div>
            <div class="col-md-1"><strong>:</strong></div>
            <div class="col-md-10">{{$event->tanggal_mulai}} to {{$event->tanggal_akhir}}</div>
          </div>
        </li>
      </ul>
      <div class="panel-footer text-right">
        <a href="{{ route('attendance.overallEventReport', ['eventId' => $event->id]) }}" class="btn btn-info btn-show">
            <i class="fa fa-book"></i> Event Report
        </a>
        <a href="{{ route('attendance.workshop', ['eventId' => $event->id]) }}" class="btn btn-success btn-show">
            <i class="fa fa-desktop"></i> View Workshop
        </a>
      </div>
    </div>
  </div>
@empty
  <div class="alert alert-secondary" role="alert">
    No event data yet, @if(auth()->user()->role_id != 1) please contact your administrator @else please input event data first in <a href="{{ route('event.index') }}">here <i class="fa fa-external-link"></i></a> @endIf
  </div>
@endForelse
</div>

<script>

$( document ).ready(function() {
  // $('.btn-show').click(function(e) {
  //   e.preventDefault();
  //   url = this.href;
  //   dt = new Date();
  //   year = dt.getFullYear();
  //   month = dt.getMonth() + 1;
  //   day = dt.getDate();
  //   hour = dt.getHours();
  //   minute = dt.getMinutes();
  //   clientsLocalTime = year +'-'+ month +'-'+ day +' '+ hour +':'+ minute +':00';
  //   url = url + '?clients_local_time=' + clientsLocalTime;
  //   window.location = url; 
  // });
    // $('div.eventButtonTable').html('<button type="button" class="btn btn-sm btn-success pull-left m-0">Tambah</button>');

    // $('#eventDtab').DataTable({
    //     // dom : '<""<"eventButtonTable">f>tp',
    // });
});

</script>

@stop

@section('libs')

<!-- <link rel="stylesheet" type="text/css" href="{{ asset('datatables/datatables.css') }}">
<script type="text/javascript" charset="utf8" src="{{ asset('datatables/datatables.js') }}"></script> -->

@stop