@extends('layouts.app-base')

@section('content')

<button onclick="window.open('{{ route('attendance.exportOverallEventReport', ['eventId' => $event->id]) }}')" class="btn btn-info btn-sm text-white text-sm-center mb-2">
    <i class="fa fa-file-excel-o"></i> Export
</button>
<a href="{{ route('attendance.index') }}" class="btn btn-danger btn-sm text-white text-sm-center mb-2">
    <i class="fa fa-arrow-circle-o-left"></i> Back
</a>

<table class="table table-sm">
    <tr>
        <td>Event</td>
        <td>{{ $event->nama }}</td>
    </tr>
    <tr>
        <td>Place</td>
        <td>{{ $event->tempat }}</td>
    </tr>
    <tr>
        <td>Time</td>
        <td>{{ $event->tanggal_mulai }} to {{ $event->tanggal_akhir }}</td>
    </tr>
    <tr>
        <td>Total Duration</td>
        <td>{{ $event->durasi }} minutes</td>
    </tr>
</table>

<div class="panel panel-primary">
    <div class="panel-heading">Attendance Event Report</div>
    <div class="panel-body">

        <table id="participantDtab" class="table table-xs table-striped table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th>Participant Number</th>
                    <th>Name</th>
                    <th>Identity Number</th>
                    <th>KTA Number</th>
                    <th>Institute</th>
                    <th>Total Time Attendance (Minutes)</th>
                    <th>Attendance Percentage</th>
                    <th>Pass Status</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($participants as $participant)
                    <tr>
                        <td>{{ $participant->kode_peserta }}</td>
                        <td>{{ $participant->nama }}</td>
                        <td>{{ $participant->no_ktp }}</td>
                        <td>{{ $participant->no_kta }}</td>
                        <td>{{ $participant->institusi }}</td>
                        <td>{{ $participant->waktu_kehadiran }}</td>
                        <td>{{ $participant->persentase_kehadiran }} %</td>
                        <td>@if($participant->persentase_kehadiran >= 75) Passed @else Not Passed @endIf</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" class="text-center text-muted">No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>


<script>

$( document ).ready(function() {
    $('#participantDtab').DataTable({
        order: [[0, 'asc'], [1, 'asc']],
        responsive: true
    });
});

</script>

@stop

@section('libs')

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('datatables-responsive/dataTables.responsive.js') }}"></script>

@stop