@extends('layouts.app-base')

@section('content')

<div class="row mb-2">
@forelse ($workshops as $workshop)
  <div class="col-sm-4">
    <div class="panel panel-primary">
      <div class="panel-body text-white bg-primary" style="height:125px; overflow-y:auto">
        <h3>{{$workshop->judul_materi}}</h3>
        <p class="card-text">{{$workshop->deskripsi}}</p>
      </div>
      <ul class="list-group">
        <li class="list-group-item">
          <div class="row">
            <div class="col-md-12"><strong>Speaker</strong></div>
          </div>
          <div class="row">
            <div class="col-md-12">{{$workshop->pengisi_materi}}</div>
          </div>
        </li>
        <li class="list-group-item">
          <div class="row">
            <div class="col-md-12"><strong>Place/Room</strong></div>
          </div>
          <div class="row">
            <div class="col-md-12">{{$workshop->tempat}}</div>
          </div>
        </li>
        <li class="list-group-item">
          <div class="row">
            <div class="col-md-12"><strong>Time</strong></div>
          </div>
          <div class="row">
            <div class="col-md-12">{{$workshop->waktu_mulai}} to {{$workshop->waktu_akhir}}</div>
          </div>
        </li>
        <li class="list-group-item">
          <div class="row">
            <div class="col-md-12"><strong>Session</strong></div>
          </div>
          <div class="row">
            <div class="col-md-12">
                <select id="session-{{ $workshop->id }}-dropdown" class="form-control">
                  @foreach (($workshop->sessions()->get()) as $session)
                    <option value="{{ $session->id }}" disabled data-start="{{ $session->waktu_mulai->format('Y-m-d H:i') }}" data-end="{{ $session->waktu_akhir->format('Y-m-d H:i') }}">
                      {{ $session->sesi }} [ {{ $session->waktu_mulai->format('H:i') }} - {{ $session->waktu_akhir->format('H:i') }} ]
                    </option>
                  @endforeach
                </select>
            </div>
          </div>
        </li>
      </ul>
      <div class="panel-footer text-right">
        <a href="{{ route('attendance.overallReport', ['workshopId' => $workshop->id]) }}" class="btn btn-sm btn-info">
            <i class="fa fa-book"></i> Attendance Report
        </a>
        <a class="btn btn-sm btn-success" onclick="redirectToScan({{ $workshop->id }})">
            <i class="fa fa-calendar-check-o"></i> Attendance
        </a>
      </div>
    </div>
  </div>
@empty
  <div class="alert alert-secondary" role="alert">
    No workshop data yet in <strong>{{ $event->nama }}</strong> event, @if(auth()->user()->role_id != 1) please contact your administrator @else please input workshop data first in <a href="{{ route('event.show', ['id' => $event->id]) }}">here <i class="fa fa-external-link"></i></a> @endIf
  </div>
@endForelse
</div>
<script>

$( document ).ready(function() {
    $('select[id^=session-] > option').each(function() {
      start = moment($(this).data('start')).subtract({ hours:1 });
      end = $(this).data('end');

      if (moment().isBetween(start, end)) {
        $(this).removeAttr('disabled');
        $(this).attr('selected', 'selected');
      }
    });
});

function redirectToScan(workshopId) {
    sessionId = $('#session-'+ workshopId +'-dropdown').val();

    if (sessionId) {
      url = "{{ route('attendance.scan', ['sessionId' => 'sessionId']) }}";
      url = url.replace('sessionId', sessionId);
      window.location.href = url;
    } else {
      alert('Please choose the workshop\'s session first');
    }
}

</script>

@stop

@section('libs')
  <script type="text/javascript" src="{{ asset('datetime-picker/moment.min.js') }}"></script>
@stop