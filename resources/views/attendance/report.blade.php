@extends('layouts.app-base')

@section('content')

<a href="{{ route('attendance.scan', ['sessionId' => $session->id]) }}" class="btn btn-danger btn-sm text-white text-sm-center mb-2">
    <i class="fa fa-arrow-circle-o-left"></i> Back
</a>

<table class="table table-sm">
    <tr>
        <td>Workshop</td>
        <td>{{ $workshop->judul_materi }}</td>
    </tr>
    <tr>
        <td>Speaker</td>
        <td>{{ $workshop->pengisi_materi }}</td>
    </tr>
    <tr>
        <td>Place</td>
        <td>{{ $workshop->tempat }}</td>
    </tr>
    <tr>
        <td>Time</td>
        <td>{{ $workshop->waktu_mulai }} to {{ $workshop->waktu_akhir }}</td>
    </tr>
    <tr>
        <td>Sesion</td>
        <td>{{ $session->sesi }}</td>
    </tr>
</table>


<div class="panel panel-primary">
    <div class="panel-heading">Attendance Session Report</div>
    <div class="panel-body">

        <table id="participantDtab" class="table table-xs table-striped table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th>Participant Number</th>
                    <th>Name</th>
                    <th>Identity Number</th>
                    <th>KTA Number</th>
                    <th>Institute</th>
                    <th>Check In</th>
                    <th>Check Out</th>
                    <th>Time Attendance (Minutes)</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($participants as $participant)
                    <tr>
                        <td>{{ $participant->kode_peserta }}</td>
                        <td>{{ $participant->nama }}</td>
                        <td>{{ $participant->no_ktp }}</td>
                        <td>{{ $participant->no_kta }}</td>
                        <td>{{ $participant->institusi }}</td>
                        <td>{{ $participant->check_in }}</td>
                        <td>{{ $participant->check_out }}</td>
                        <td>{{ $participant->waktu_kehadiran }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" class="text-center text-muted">No data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>

<script>

$( document ).ready(function() {
    $('#participantDtab').DataTable({
        order: [[0, 'asc'], [1, 'asc']],
        responsive: true
    });
});

</script>

@stop

@section('libs')

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('datatables-responsive/dataTables.responsive.js') }}"></script>

@stop