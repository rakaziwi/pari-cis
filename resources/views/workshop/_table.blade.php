<a href="{{ route('workshop.create', ['id' => $event->id]) }}" class="btn btn-success btn-sm mb-2">
    <i class="fa fa-plus-circle"></i> Create New
</a>
<table id="workshopDtab" class="table table-xs table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th width="100px"></th>
            <th>Workshop Title</th>
            <th>Speaker</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Place/Room</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($workshops as $workshop)
            <tr>
                <td class="align-middle">
                    <span>
                        <a href="{{ route('workshop.show', ['id' => $workshop->id]) }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="{{ route('workshop.edit', ['id' => $workshop->id]) }}" class="btn btn-outline-primary btn-sm">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="{{ route('workshop.delete', ['id' => $workshop->id]) }}" class="btn btn-outline-danger btn-sm">
                            <i class="fa fa-trash"></i>
                        </a>
                    </span>
                </td>
                <td>{{ $workshop->judul_materi }}</td>
                <td>{{ $workshop->pengisi_materi }}</td>
                <td>{{ $workshop->waktu_mulai }}</td>
                <td>{{ $workshop->waktu_akhir }}</td>
                <td>{{ $workshop->tempat }}</td>
                <td>{{ $workshop->deskripsi }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="7" class="text-center text-muted">No data</td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
            </tr>
        @endforelse
    </tbody>
</table>