@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary mb-4">
    <div class="panel-heading">{{ $workshop->judul_materi }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2">Speaker</div>
            <div class="col-sm-10 text-muted">{{ $workshop->pengisi_materi }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Workshop Time</div>
            <div class="col-sm-10 text-muted">{{ $workshop->waktu_mulai }} to {{ $workshop->waktu_akhir }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Place/Room</div>
            <div class="col-sm-10 text-muted">{{ $workshop->tempat }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Description</div>
            <div class="col-sm-10 text-muted">{{ $workshop->deskripsi }}</div>
        </div>
        <div class="text-right">
            <a href="{{ route('event.show', ['id' => $workshop->event_id]) }}" class="btn btn-danger btn-sm text-white text-sm-center mt-2">
                <i class="fa fa-arrow-circle-o-left"></i> Back
            </a>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">Workshop's Session</div>
    <div class="panel-body">
        @include('session._table')
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">Workshop's Participants</div>
    <div class="panel-body">
        @include('participant._table')
    </div>
</div>

<script>

$( document ).ready(function() {
    $('#sessionDtab').DataTable({
        bLengthChange: false,
        columnDefs: [
            { orderable: false, targets: 0 }
        ],
        order: [[2, 'asc']],
        pageLength: 5,
        responsive: true
    });

    $('#participantDtab').DataTable({
        bLengthChange: false,
        columnDefs: [
            { orderable: false, targets: 0 }
        ],
        order: [[1, 'asc']],
        responsive: true
    });
});

</script>

@stop

@section('libs')

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('datatables-responsive/dataTables.responsive.js') }}"></script>

@stop