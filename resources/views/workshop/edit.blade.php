@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Edit Workshop</div>
    <div class="panel-body">

    {!! Form::model($workshop, ['route' => ['workshop.update', $workshop->id], 'method' => 'PATCH']) !!}

        @include('workshop._workshop', ['submitLabel' => 'Update'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop