<div class="form-group">
    {!! Form::hidden('id', null) !!}
    {!! Form::hidden('event_id', null) !!}
    {!! Form::label('judul_materi', 'Workshop Title:') !!}
    {!! Form::text('judul_materi', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('pengisi_materi', 'Speaker:') !!}
    {!! Form::text('pengisi_materi', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('waktu_mulai', 'Workshop Time:') !!}
    {!! Form::text('waktu_mulai', \Carbon\Carbon::now(), ['class' => 'form-control', $readonly]) !!}
    @if ($readonly != '') <h5 class="text-info">*To change workshop time, please delete all workshop's sessions</h5> @endIf
</div>
<div class="form-group">
    {!! Form::label('tempat', 'Place/Room:') !!}
    {!! Form::text('tempat', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('deskripsi', 'Description:') !!}
    {!! Form::text('deskripsi', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit($submitLabel, ['class' => 'btn btn-primary form-control']) !!}
    <a onclick="window.history.back()" type="button" class="btn btn-danger form-control text-white">Cancel</a>
</div>

<script>
    $(function() {
        console.log('{{$event->tanggal_mulai}}' + ' 07:00');
        tanggalMulai = '{{$event->tanggal_mulai}}' + ' 07:00';
        tanggalAkhir = '{{$event->tanggal_mulai}}' + ' 10:00';
        waktuMulai = '{{ $workshop->waktu_mulai }}';
        waktuAkhir = '{{ $workshop->waktu_akhir }}';
        startDate = (waktuMulai != '') ? waktuMulai : tanggalMulai;
        endDate = (waktuAkhir != '') ? waktuAkhir : tanggalAkhir;

        $('#waktu_mulai').daterangepicker({
            timePicker: true,
            timePicker24Hour: true,
            minDate: '{{ $event->tanggal_mulai }}' + ' 00:00',
            maxDate: '{{ $event->tanggal_akhir }}' + ' 23:59',
            // startDate: moment('{{ $event->tanggal_mulai }}').startOf('hour').add(7, 'hour'),
            // endDate: moment('{{ $event->tanggal_mulai }}').startOf('hour').add(10, 'hour'),
            startDate: startDate,
            endDate: endDate,
            locale: {
                format: 'YYYY/MM/DD HH:mm'
            }
        }).on('show.daterangepicker', function(ev, picker) {
            if ('{{ $readonly }}' != '') {
                $('.daterangepicker').hide();
            }
        });
    });
</script>

@section('libs')
    <script type="text/javascript" src="{{ asset('datetime-picker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('datetime-picker/daterangepicker.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('datetime-picker/daterangepicker.css') }}" />
@stop