@extends('layouts.app-base')

@section('content')

<table id="workshopDtab" class="table-striped" style="width:100%">
    <thead>
        <tr>
            <th></th>
            <th>Workshop Title</th>
            <th>Speaker</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($workshops as $workshop)
            <tr>
                <td class="align-middle">
                    <span>
                        <a href="{{ route('workshop.show', ['id' => $workshop->id]) }}" type="button" class="btn btn-primary btn-sm">
                            <i class="fa fa-clipboard-list"></i>
                        </a>
                    </span>
                </td>
                <td>{{ $workshop->judul_materi }}</td>
                <td>{{ $workshop->pengisi_materi }}</td>
                <td>{{ $workshop->waktu_mulai }}</td>
                <td>{{ $workshop->waktu_selesai }}</td>
                <td>{{ $workshop->deskripsi }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="text-center text-muted">No data</td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
            </tr>
        @endforelse
    </tbody>
</table>

<script>

// $( document ).ready(function() {
//     $('div.workshopButtonTable').html('<button type="button" class="btn btn-sm btn-success pull-left m-0">Tambah</button>');

//     $('#workshopDtab').DataTable({
//         dom : '<""<"workshopButtonTable">f>tp',
//     });
// });

</script>

@stop