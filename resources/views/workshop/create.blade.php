@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Create Workshop</div>
    <div class="panel-body">

    {!! Form::model($workshop, ['route' => 'workshop.store']) !!}

        @include('workshop._workshop', ['submitLabel' => 'Save'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop