@extends('layouts.app-base')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">Create Session</div>
    <div class="panel-body">

    {!! Form::model($workshopSession, ['route' => 'session.store']) !!}

        @include('session._session', ['submitLabel' => 'Save'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop