@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Edit Session</div>
    <div class="panel-body">

    {!! Form::model($session, ['route' => ['session.update', $session->id], 'method' => 'PATCH']) !!}

        @include('session._session', ['submitLabel' => 'Update'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop