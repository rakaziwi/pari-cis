<div class="form-group">
    {!! Form::hidden('id', null) !!}
    {!! Form::hidden('jadwal_materi_id', null) !!}
    {!! Form::label('sesi', 'Session:') !!}
    {!! Form::text('sesi', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('waktu_mulai', 'Session Time:') !!}
    {!! Form::text('waktu_mulai', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit($submitLabel, ['class' => 'btn btn-primary form-control']) !!}
    <a onclick="window.history.back()" type="button" class="btn btn-danger form-control text-white">Cancel</a>
</div>

<script>

$('#waktu_mulai').daterangepicker({
    timePicker: true,
    timePicker24Hour: true,
    timePickerSeconds: false,
    minDate: '{{ $minTime }}',
    maxDate: '{{ $maxTime }}',
    startDate: '{{ $startTime }}',
    endDate: '{{ $endTime }}',
    locale: {
        format: 'HH:mm'
    }
}).on('show.daterangepicker', function (ev, picker) {
    picker.container.find(".calendar-table").hide();
});

</script>

@section('libs')
    <script type="text/javascript" src="{{ asset('datetime-picker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('datetime-picker/daterangepicker.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('datetime-picker/daterangepicker.css') }}" />
@stop