<a href="{{ route('session.create', ['workshopId' => $workshop->id ]) }}" class="btn btn-success btn-sm mb-2">
    <i class="fa fa-plus-circle"></i> Create New Session
</a>
<table id="sessionDtab" class="table table-xs table-striped table-bordered" style="width=100%">
    <thead>
        <tr>
            <th width="50px"></th>
            <th>Session</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Duration (minutes)</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($sessions as $session)
            <tr>
                <td class="align-middle">
                    <span>
                        <a href="{{ route('session.edit', ['id' => $session->id]) }}" class="btn btn-outline-primary btn-sm">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="{{ route('session.delete', ['id' => $session->id]) }}" class="btn btn-outline-danger btn-sm">
                            <i class="fa fa-trash"></i>
                        </a>
                    </span>
                </td>
                <td>{{ $session->sesi }}</td>
                <td>{{ $session->waktu_mulai }}</td>
                <td>{{ $session->waktu_akhir }}</td>
                <td>{{ $session->durasi }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="5" class="text-center text-muted">No data</td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
            </tr>
        @endforelse
    </tbody>
</table>