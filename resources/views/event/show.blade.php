@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary mb-4">
    <div class="panel-heading">{{ $event->nama }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2">Event Date</div>
            <div class="col-sm-10 text-muted">{{ $event->tanggal_mulai }} to {{ $event->tanggal_akhir }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Place</div>
            <div class="col-sm-10 text-muted">{{ $event->tempat }}</div>
        </div>
        <div class="row">
            <div class="col-sm-2">Description</div>
            <div class="col-sm-10 text-muted">{{ $event->keterangan }}</div>
        </div>
        <div class="text-right">
            <a href="{{ route('event.index') }}" class="btn btn-danger btn-sm text-white text-sm-center mt-2">
                <i class="fa fa-arrow-circle-o-left"></i> Back
            </a>
        </div>
    </div>
</div>

<div class="panel panel-primary mb-4">
    <div class="panel-heading">Event's Workshop</div>
    <div class="panel-body">
        @include('workshop._table')
    </div>
</div>

<script>

$( document ).ready(function() {
    $('#workshopDtab').DataTable({
        bLengthChange: false,
        columnDefs: [
            { orderable: false, targets: 0 }
        ],
        order: [[3, 'asc'], [1, 'asc']],
        responsive: true
    });
});

</script>

@stop

@section('libs')

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('datatables-responsive/dataTables.responsive.js') }}"></script>

@stop