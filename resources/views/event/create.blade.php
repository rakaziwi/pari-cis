@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Create Event</div>
    <div class="panel-body">

    {!! Form::model($event, ['route' => 'event.store']) !!}

        @include('event._event', ['submitLabel' => 'Save'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop