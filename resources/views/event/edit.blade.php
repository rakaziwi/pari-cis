@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Edit Event</div>
    <div class="panel-body">

    {!! Form::model($event, ['route' => ['event.update', $event->id], 'method' => 'PATCH']) !!}

        @include('event._event', ['submitLabel' => 'Update'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop