<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::hidden('id', null) !!}
            {!! Form::label('nama', 'Event:') !!}
            {!! Form::text('nama', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('tanggal_mulai', 'Event Date:') !!}
            {!! Form::text('tanggal_mulai', \Carbon\Carbon::now(), ['class' => 'form-control', $readonly]) !!}
            @if ($readonly != '') <h5 class="text-info">*To change event time, please delete all event's workshops</h5> @endIf
        </div>
        <div class="form-group">
            {!! Form::label('tempat', 'Place:') !!}
            {!! Form::text('tempat', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('keterangan', 'Description:') !!}
            {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit($submitLabel, ['class' => 'btn btn-primary form-control']) !!}
            <a onclick="window.history.back()" class="btn btn-danger form-control text-white">Cancel</a>
        </div>
    </div>
</div>

<script>
    $(function() {
        // hack when edit 
        tanggalMulai = '{{ $event->tanggal_mulai }}';
        tanggalAkhir = '{{ $event->tanggal_akhir }}';
        $('#tanggal_mulai').daterangepicker({
            timePicker: false,
            startDate: (tanggalMulai != '') ? tanggalMulai :moment().startOf('day'),
            endDate: (tanggalAkhir != '') ? tanggalAkhir : moment().startOf('day'), // hack when edit
            locale: {
                format: 'YYYY/MM/DD'
            }
        }).on('show.daterangepicker', function(ev, picker) {
            if ('{{ $readonly }}' != '') {
                $('.daterangepicker').hide();
            }
        });;
    });
</script>

@section('libs')
    <script type="text/javascript" src="{{ asset('datetime-picker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('datetime-picker/daterangepicker.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('datetime-picker/daterangepicker.css') }}" />
@stop