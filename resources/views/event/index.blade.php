@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Event</div>
    <div class="panel-body">
        <a href="{{ route('event.create') }}" class="btn btn-success btn-sm mb-2 small">
            <i class="fa fa-plus-circle"></i> Create New
        </a>
        <table id="eventDtab" class="table table-xs table-striped table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th class="col-md-2"></th>
                    <th class="col-md-2">Name</th>
                    <th class="col-md-2">Start Date</th>
                    <th class="col-md-2">End Date</th>
                    <th class="col-md-2">Place</th>
                    <th class="col-md-2">Description</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($events as $event)
                    <tr>
                        <td class="align-middle">
                            <span>
                                <a href="{{ route('event.show', ['id' => $event->id]) }}" class="btn btn-outline-info btn-sm" title="Show Detail">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a href="{{ route('event.edit', ['id' => $event->id]) }}" class="btn btn-outline-primary btn-sm" title="Edit">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a onclick="return confirm('Are you sure?')" href="{{ route('event.delete', ['id' => $event->id]) }}" class="btn btn-outline-danger btn-sm" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </span>
                        </td>
                        <td>{{ $event->nama }}</td>
                        <td>{{ $event->tanggal_mulai }}</td>
                        <td>{{ $event->tanggal_akhir }}</td>
                        <td>{{ $event->tempat }}</td>
                        <td>{{ $event->keterangan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center text-muted">No data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<script>

$( document ).ready(function() {
    $('#eventDtab').DataTable({
        bLengthChange: false,
        columnDefs: [
            { orderable: false, targets: 0 }
        ],
        order: [[2, 'asc'], [1, 'asc']],
        responsive: true
    });
});

</script>

@stop

@section('libs')

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('datatables-responsive/dataTables.responsive.js') }}"></script>

@stop