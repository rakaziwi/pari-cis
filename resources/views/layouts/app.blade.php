<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PARI CIS</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}">

    <!-- Styles -->
    <link href="{{ asset('bootstrap-3.3.7/css/bootstrap.css') }}" rel="stylesheet">

    <!-- JS -->
    <script type="text/javascript" src="{{ asset('jQuery-3.3.1/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bootstrap-3.3.7/js/bootstrap.min.js') }}"></script>
</head>
<body>
    
    <div class="row">
        <div class="col-md-12">@include('layouts.top-navbar')</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="container">
                @yield('content')
            </div>
        </div>
    </div>
</body>
</html>
