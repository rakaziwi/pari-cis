@if (Session::has('flash_notification_message'))

    <div class="alert
                alert-{{ Session::has('flash_notification_level') ? session('flash_notification_level') : 'danger' }}
                {{ Session::has('flash_notification_important') ? 'alert-important' : '' }}"
                role="alert"
    >
        @if (Session::has('flash_notification_important'))
            <button type="button"
                    class="close"
                    data-dismiss="alert"
                    aria-hidden="true"
            >&times;</button>
        @endif

        {{ session('flash_notification_message') }}
    </div>

@endIf