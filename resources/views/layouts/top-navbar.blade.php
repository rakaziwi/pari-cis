<nav class="navbar navbar-expand-sm bg-primary navbar-dark fixed-top shadow">
  <a class="navbar-brand" href="#">P-CiS</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('welcome') }}">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('attendance.index') }}">Workshop Attendance</a>
      </li>
      @if (Auth::user()->role_id == 1)
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          Master
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="{{ route('event.index') }}">Event</a>
          <a class="dropdown-item" href="{{ route('participant.index') }}">Participant</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('user.index') }}">Admin</a>
      </li>
      @endIf
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a href="#" class="nav-link" onclick="event.preventDefault();$('#logout-form').submit();">Logout</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </div>
</nav>