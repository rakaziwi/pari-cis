<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;background:#337ab7;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ route('dashboard') }}" style="color:#fff">PARI Courses Integrated System</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:#fff">
                {{ auth()->user()->name }}<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#" onclick="event.preventDefault();$('#logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
            </ul>
            <form id="logout-form" action="{{ route('logout') }}" method="post" style="display:none;">{{ csrf_field() }}</form>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="{{ route('dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="{{ route('attendance.index') }}"><i class="fa fa-calendar-check-o fa-fw"></i> Workshop Attendance</a>
                </li>
                @if (auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                <li>
                    <a href="#"><i class="fa fa-calendar fa-fw"></i> Event Management<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ route('event.index') }}">Events</a>
                        </li>
                        <li>
                            <a href="{{ route('participant.index') }}">Participants</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                @if (auth()->user()->role_id == 3)
                <li>
                    <a href="{{ route('user.index') }}"><i class="fa fa-users fa-fw"></i> User Management</a>
                </li>
                @endIf
                @endIf
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>