@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Participant</div>
    <div class="panel-body">

        <a href="{{ route('participant.create') }}" class="btn btn-success btn-sm mb-2">
            <i class="fa fa-plus-circle"></i> Create New
        </a>
        <table id="participantDtab" class="table table-xs table-striped table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th width="125px"></th>
                    <th>Participant Number</th>
                    <th>Name</th>
                    <th>Identity Number</th>
                    <th>KTA Number</th>
                    <th>Institute</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Sex</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($participants as $participant)
                    <tr>
                        <td class="align-middle">
                            <span>
                                <a href="{{ route('participant.edit', ['id' => $participant->id]) }}" class="btn btn-outline-primary btn-sm">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a onclick="return confirm('Are you sure?')" href="{{ route('participant.delete', ['id' => $participant->id]) }}" class="btn btn-outline-danger btn-sm">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </span>
                        </td>
                        <td>{{ $participant->kode_peserta }}</td>
                        <td>{{ $participant->nama }}</td>
                        <td>{{ $participant->no_ktp }}</td>
                        <td>{{ $participant->no_kta }}</td>
                        <td>{{ $participant->institusi }}</td>
                        <td>{{ $participant->email }}</td>
                        <td>{{ $participant->no_telp }}</td>
                        <td>{{ $participant->jk }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="9" class="text-center text-muted">No data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>

<script>

$( document ).ready(function() {
    $('#participantDtab').DataTable({
        bLengthChange: false,
        columnDefs: [
            { orderable: false, targets: 0 }
        ],
        order: [[1, 'asc']],
        responsive: true
    });
});

</script>

@stop

@section('libs')

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('datatables-responsive/dataTables.responsive.js') }}"></script>

@stop