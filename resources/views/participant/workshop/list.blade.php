@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Add Participant to Workshop</div>
    <div class="panel-body">

        {!! Form::open(['route' => 'attendance.register', 'id' => 'register-form']) !!}

        <a class="btn btn-success btn-sm mb-2 text-white" onclick="register()">
            <i class="fa fa-user-plus"></i> Add Participant
        </a>
        <a href="{{ route('workshop.show', ['id' => $workshopId]) }}" class="btn btn-danger btn-sm text-white text-sm-center mb-2">
            <i class="fa fa-arrow-circle-o-left"></i> Back
        </a>

        {!! Form::hidden('workshop_id', $workshopId) !!}

        <table id="participantDtab" class="table table-xs table-striped table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th>
                        {!! Form::checkbox('checkAll', null, null, ['id' => 'checkAll']); !!}
                    </th>
                    <th>Participant Number</th>
                    <th>Name</th>
                    <th>Identity Number</th>
                    <th>KTA Number</th>
                    <th>Institute</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Sex</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($participants as $participant)
                    <tr>
                        <td class="text-center">
                            {!! Form::checkbox('participants[]', $participant->id); !!}
                        </td>
                        <td>{{ $participant->kode_peserta }}</td>
                        <td>{{ $participant->nama }}</td>
                        <td>{{ $participant->no_ktp }}</td>
                        <td>{{ $participant->no_kta }}</td>
                        <td>{{ $participant->institusi }}</td>
                        <td>{{ $participant->email }}</td>
                        <td>{{ $participant->no_telp }}</td>
                        <td>{{ $participant->jk }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="9" class="text-center text-muted">No data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        {!! Form::close() !!}
    </div>
</div>

<script>

$( document ).ready(function() {
    participantDtab = $('#participantDtab').DataTable({
        bAutoWidth:false,
        bLengthChange: false,
        columnDefs: [
            { orderable: false, targets: 0 }
        ],
        order: [[1, 'asc']],
        responsive: true
    });

    $("#checkAll").on('click', function () {
        // participantDtab
        //     .column(0)
        //     .nodes()
        //     .to$()
        //     .find('input[type=checkbox]')
        //     .prop('checked', this.checked);
        
        var checked = $(this).is(":checked");
 
        $("input", participantDtab.rows({search:'applied'}).nodes()).each(function() {
            if (checked) {
                $(this).attr("checked", true);
            } else {
                $(this).attr("checked", false);
            }
        });
    });
});

function register() {
    var $form = $('#register-form');

    // Iterate over all checkboxes in the table
    participantDtab.$('input[type="checkbox"]').each(function() {
        // If checkbox doesn't exist in DOM
        if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
                // Create a hidden element 
                $form.append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', this.name)
                        .val(this.value)
                );
            }
        } 
    });          
    $('#register-form').submit();
}

</script>

@stop

@section('libs')

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('datatables-responsive/dataTables.responsive.js') }}"></script>

@stop