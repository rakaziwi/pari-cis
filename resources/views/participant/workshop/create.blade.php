@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Create New Participant and Map to Workshop</div>
    <div class="panel-body">

    {!! Form::model($participant, ['route' => ['participant.storeInWorkshop', $workshopId]]) !!}

        @include('participant._participant', ['submitLabel' => 'Save'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop