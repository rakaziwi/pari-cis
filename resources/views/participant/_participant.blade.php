<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::hidden('id', null) !!}
            {!! Form::label('kode_peserta', 'Participant Code:') !!}
            {!! Form::text('kode_peserta', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('no_ktp', 'Identity Number:') !!}
            {!! Form::text('no_ktp', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('nama', 'Name:') !!}
            {!! Form::text('nama', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('no_kta', 'KTA Number:') !!}
            {!! Form::text('no_kta', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('institusi', 'Institute:') !!}
            {!! Form::text('institusi', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('no_telp', 'Phone Number:') !!}
            {!! Form::text('no_telp', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('jk', 'Sex:') !!}
            {!! Form::select('jk', ['L' => 'Male', 'P' => 'Female'], 'L', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('barcode', 'Barcode:') !!}
            {!! Form::text('barcode', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit($submitLabel, ['class' => 'btn btn-primary form-control']) !!}
            <a onclick="window.history.back()" class="btn btn-danger form-control text-white">Cancel</a>
        </div>
    </div>
</div>