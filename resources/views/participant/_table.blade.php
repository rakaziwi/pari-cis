<a href="{{ route('participant.createInWorkshop', ['workshopId' => $workshop->id ]) }}" class="btn btn-success btn-sm mb-2">
    <i class="fa fa-plus-circle"></i> Create New Participant
</a>
<a href="{{ route('participant.showAvailable', ['workshopId' => $workshop->id ]) }}" class="btn btn-warning btn-sm mb-2 text-white">
    <i class="fa fa-user-plus"></i> Add From Existing
</a>
<table id="participantDtab" class="table table-xs table-striped table-bordered" style="width=100%">
    <thead>
        <tr>
            <th></th>
            <th>Participant Number</th>
            <th>Name</th>
            <th>Institution</th>
            <th>Attendance Duration (Minutes)</th>
            <th>Attendance Percentation (%)</th>
            <th>Certificate</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($participants as $participant)
            <tr>
                <td class="align-middle">
                    <span>
                        <a onclick="return confirm('Are you sure to remove participant?')" href="{{ route('attendance.delete', ['id' => $participant->id]) }}" class="btn btn-outline-danger btn-sm" title="Remove Participant">
                            <i class="fa fa-user-times"></i>
                        </a>
                    </span>
                </td>
                <td>{{ $participant->kode_peserta }}</td>
                <td>{{ $participant->nama }}</td>
                <td>{{ $participant->institusi }}</td>
                <td>{{ $participant->waktu_kehadiran }}</td>
                <td>{{ $participant->persentase_kehadiran }}</td>
                <td class="text-center">
                    {!! Form::checkbox('certificate_'. $participant->peserta_id, '1', ($participant->cetak_sertifikat) ? true : false, ['disabled']); !!}
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="7" class="text-center text-muted">No data</td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
                <td style="display: none;"></td>
            </tr>
        @endforelse
    </tbody>
</table>