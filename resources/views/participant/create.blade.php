@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Create Participant</div>
    <div class="panel-body">

    {!! Form::model($participant, ['route' => 'participant.store']) !!}

        @include('participant._participant', ['submitLabel' => 'Save'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop