@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Edit Participant</div>
    <div class="panel-body">

    {!! Form::model($participant, ['route' => ['participant.update', $participant->id], 'method' => 'PATCH']) !!}

        @include('participant._participant', ['submitLabel' => 'Update'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop