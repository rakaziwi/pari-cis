<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::hidden('id', null) !!}
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('password', 'Password:') !!}
            {!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('role', 'Role:') !!}
            {!! Form::select('role_id', $roles, null, ['placeholder' => 'Pick a role...', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit($submitLabel, ['class' => 'btn btn-primary form-control']) !!}
            <a onclick="window.history.back()" type="button" class="btn btn-danger form-control text-white">Cancel</a>
        </div>
    </div>
</div>