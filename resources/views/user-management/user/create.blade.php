@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Create User</div>
    <div class="panel-body">

    {!! Form::open(['route' => 'user.store']) !!}

        @include('user-management.user._user', ['roles' => $roles, 'submitLabel' => 'Save'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop