@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">User</div>
    <div class="panel-body">

        <a href="{{ action('UserManagement\UserController@create') }}" class="btn btn-success btn-sm mb-2">
            <i class="fa fa-plus-circle"></i> Create New
        </a>
        <table id="userDtab" class="table table-xs table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($users as $user)
                    <tr>
                        <td class="align-middle">
                            <span>
                                <a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-outline-primary btn-sm">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a onclick="return confirm('Are you sure?')" href="{{ route('user.delete', ['id' => $user->id]) }}" class="btn btn-outline-danger btn-sm">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </span>
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role_name }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center text-muted">No data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>

<script>

// $( document ).ready(function() {
//     $('div.userButtonTable').html('<button type="button" class="btn btn-sm btn-success pull-left m-0">Tambah</button>');

//     $('#userDtab').DataTable({
//         dom : '<""<"userButtonTable">f>tp',
//     });
// });

</script>

@stop