@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Edit User</div>
    <div class="panel-body">

    {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PATCH']) !!}

        @include('user-management.user._user', ['roles' => $roles, 'submitLabel' => 'Update'])

    {!! Form::close() !!}

    @include('errors.validation-error')

    </div>
</div>

@stop