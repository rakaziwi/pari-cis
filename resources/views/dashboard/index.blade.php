@extends('layouts.app-base')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Attendance Resume</div>
    <div class="panel-body">

        <table id="resumeDtab" class="table table-xs table-striped table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th>Event</th>
                    <th>Event Start Date</th>
                    <th>Event End Date</th>
                    <th>Workshop</th>
                    <th>Session</th>
                    <th>Number of Attendance</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($attendanceResumes as $resume)
                    <tr>
                        <td>{{ $resume->nama_event }}</td>
                        <td>{{ $resume->tanggal_mulai_event }}</td>
                        <td>{{ $resume->tanggal_akhir_event }}</td>
                        <td>{{ $resume->judul_materi }}</td>
                        <td>{{ $resume->sesi }}</td>
                        <td>{{ $resume->jumlah_peserta_hadir }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center text-muted">No data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>

<script>

$( document ).ready(function() {
    $('#resumeDtab').DataTable({
        bLengthChange: false,
        order: [[1, 'asc'], [0, 'asc'], [3, 'asc'], [4, 'asc']],
        responsive: true
    });
});

</script>

@stop

@section('libs')

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('datatables-responsive/dataTables.responsive.js') }}"></script>

@stop