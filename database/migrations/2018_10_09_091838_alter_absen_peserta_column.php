<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAbsenPesertaColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('absen_peserta', function (Blueprint $table) {
            if (Schema::hasColumn('absen_peserta', 'cetak_sertifikat')) {
                $table->dropColumn('cetak_sertifikat');
            }
            if (Schema::hasColumn('absen_peserta', 'jadwal_materi_id')) {
                $table->dropForeign('absen_peserta_jadwal_materi_id_foreign');
                $table->dropColumn('jadwal_materi_id');
            }
            $table->bigInteger('sesi_materi_id')->unsigned();
            $table->foreign('sesi_materi_id')->references('id')->on('sesi_materi')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('absen_peserta', function (Blueprint $table) {
            if (Schema::hasColumn('absen_peserta', 'sesi_materi_id')) {
                $table->dropForeign('absen_peserta_sesi_materi_id_foreign');
                $table->dropColumn('sesi_materi_id');
            }
            $table->integer('jadwal_materi_id')->unsigned();
            $table->foreign('jadwal_materi_id')->references('id')->on('jadwal_materi');
            $table->boolean('cetak_sertifikat')->default(false);
        });
    }
}
