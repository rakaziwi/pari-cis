<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSesiMateriRelationKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sesi_materi', function (Blueprint $table) {
            $table->foreign('jadwal_materi_id')->references('id')->on('jadwal_materi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sesi_materi', function (Blueprint $table) {
            $table->dropForeign('sesi_materi_jadwal_materi_id_foreign');
        });
    }
}
