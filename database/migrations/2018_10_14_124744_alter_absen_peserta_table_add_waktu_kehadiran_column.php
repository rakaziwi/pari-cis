<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAbsenPesertaTableAddWaktuKehadiranColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('absen_peserta', function (Blueprint $table) {
            $table->smallInteger('waktu_kehadiran')->unsigned()->nullable()->comment('in minutes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('absen_peserta', function (Blueprint $table) {
            $table->dropColumn('waktu_kehadiran');
        });
    }
}
