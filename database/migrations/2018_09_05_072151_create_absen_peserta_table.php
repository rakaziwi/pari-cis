<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsenPesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absen_peserta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('jadwal_materi_id')->unsigned();
            $table->bigInteger('peserta_id')->unsigned();
            $table->dateTime('check_in')->nullable();
            $table->dateTime('check_out')->nullable();
            $table->boolean('cetak_sertifikat')->default(false);
            $table->string('catatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absen_peserta');
    }
}
