<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kode_peserta');
            $table->string('no_ktp', 30)->nullable();
            $table->string('nama');
            $table->string('no_kta', 30)->nullable();
            $table->string('institusi', 100)->nullable();
            $table->string('email')->nullable();
            $table->string('no_telp', 30)->nullable();
            $table->string('jk', 1);
            $table->string('created_by', 32)->nullable();
            $table->string('updated_by', 32)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peserta');
    }
}
