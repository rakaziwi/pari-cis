<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaMateriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_materi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('jadwal_materi_id')->unsigned();
            $table->bigInteger('peserta_id')->unsigned();
            $table->smallInteger('waktu_kehadiran')->unsigned()->nullable()->comment('in minutes');
            $table->tinyInteger('persentase_kehadiran')->unsigned()->nullable();
            $table->boolean('cetak_sertifikat')->default(false);
            $table->string('catatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peserta_materi');
    }
}
