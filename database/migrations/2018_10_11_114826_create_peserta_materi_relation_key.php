<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaMateriRelationKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peserta_materi', function (Blueprint $table) {
            $table->foreign('jadwal_materi_id')->references('id')->on('jadwal_materi');
            $table->foreign('peserta_id')->references('id')->on('peserta');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peserta_materi', function (Blueprint $table) {
            $table->dropForeign('peserta_materi_jadwal_materi_id_foreign');
            $table->dropForeign('peserta_materi_peserta_id_foreign');
        });
    }
}
