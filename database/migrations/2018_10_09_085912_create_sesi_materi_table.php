<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSesiMateriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sesi_materi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('jadwal_materi_id')->unsigned();
            $table->string('sesi');
            $table->smallInteger('durasi')->unsigned()->comment('in minute');
            $table->dateTime('waktu_mulai')->nullable();
            $table->dateTime('waktu_akhir')->nullable();
            $table->string('status', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sesi_materi');
    }
}
