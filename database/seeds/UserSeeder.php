<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'superadmin', 'email' => 'super.admin@paricis.com', 'password' => app('hash')->make('p@r1c15!'), 'role_id' => 3],
            ['name' => 'administrator', 'email' => 'administrator@paricis.com', 'password' => app('hash')->make('p@r1c15!'), 'role_id' => 1],
        ];

        foreach ($users as $user) {
            DB::table('users')->insert($user);
        }
    }
}
