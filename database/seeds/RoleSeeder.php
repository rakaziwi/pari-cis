<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
			['name' => 'admin', 'description' => 'Admin of Pari CIS', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'deleted_at' => null],
			['name' => 'user', 'description' => 'User of Pari CIS', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'deleted_at' => null],
			['name' => 'superadmin', 'description' => 'Super Admin of Pari CIS', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'deleted_at' => null],
        ];
        
        foreach ($roles as $role) {
            DB::table('role')->insert($role);
        }
    }
}
