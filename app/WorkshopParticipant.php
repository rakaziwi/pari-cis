<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkshopParticipant extends Model
{
    protected $table = 'peserta_materi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jadwal_materi_id',
        'peserta_id',
        'waktu_kehadiran',
        'persentase_kehadiran',
        'cetak_sertifikat',
        'catatan'
    ];
}
