<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $table = 'peserta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_peserta',
        'no_ktp',
        'nama',
        'no_kta',
        'institusi',
        'email',
        'no_telp',
        'jk',
        'barcode',
        'created_by',
        'updated_by'
    ];
}
