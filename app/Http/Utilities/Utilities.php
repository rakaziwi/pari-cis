<?php

namespace App\Http\Utilities;

use App\ParticipantAttendance;
use App\WorkshopParticipant;
use App\WorkshopSchedule;
use App\WorkshopSession;
use App\Http\Requests\WorkshopRequest;
use Illuminate\Http\Request;

trait Utilities
{
    /**
     * Insert registered participant(s) in passed workshop id
     *
     * @param  int      $workshopId
     * @param  bigInt   $sessionId
     * @return \Illuminate\Http\Response
     */
    public function generateAttendance($workshopId, $sessionId = null, $participantId = null)
    {
        $participantAttendances = [];
        if ($sessionId != null && $participantId == null) {
            $workshopParticipants = WorkshopParticipant::where('jadwal_materi_id', $workshopId)->get();

            if ($workshopParticipants) {
                foreach ($workshopParticipants as $participant) {
                    array_push($participantAttendances, [
                        'sesi_materi_id' => $sessionId,
                        'peserta_id' => $participant->peserta_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        } else if ($sessionId == null && $participantId != null) {
            $workshopSessions = WorkshopSession::where('jadwal_materi_id', $workshopId)->get();

            if ($workshopSessions) {
                foreach ($workshopSessions as $session) {
                    array_push($participantAttendances, [
                        'sesi_materi_id' => $session->id,
                        'peserta_id' => $participantId,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }

        ParticipantAttendance::insert($participantAttendances);

        return "success";
    }

    /**
     * Generate api's response
     *
     * @param  boolean  $success
     * @param  string   $message
     * @param  int      $httpCode
     * @return \Illuminate\Http\Response
     */
    public function generateResponse($success = false, $message = null, $httpCode = null){
        if ($success != null){
            $httpCode = ($httpCode != null) ? $httpCode : ($success == false) ? 400 : 200;
            $response = response()->json([
                'success' => $success,
                'message' => $message
            ], $httpCode);

            return $response;
        } else {
            return "Please insert the success parameter";
        }
    }


    /**
     * Recalculate workshop participant's spent time total session
     *
     * @param  int      $workshopId
     * @param  bigInt   $participantId
     * @return \Illuminate\Http\Response
     */
    function updateAttendanceParticipantTime($workshopId, $participantId = null) {
        $sessionTotalDuration = WorkshopSession::where('jadwal_materi_id', $workshopId)->sum('durasi');

        if ($participantId != null) {
            $participantTime = ParticipantAttendance::where([
                    ['sesi_materi.jadwal_materi_id', $workshopId],
                    ['absen_peserta.peserta_id', $participantId]])
                ->join('sesi_materi', 'absen_peserta.sesi_materi_id', 'sesi_materi.id')
                ->sum('absen_peserta.waktu_kehadiran');

            $participant = WorkshopParticipant::where([
                ['jadwal_materi_id', $workshopId],
                ['peserta_id', $participantId]
            ])->first();
            $data['waktu_kehadiran'] = $participantTime;
            $attendancePercentage = $participantTime / $sessionTotalDuration * 100;
            $data['persentase_kehadiran'] = ($attendancePercentage > 100) ? 100 : $attendancePercentage;
            $participant->update($data);
        } else {
            $participants = WorkshopParticipant::where('jadwal_materi_id', $workshopId)->get();

            foreach ($participants as $participant) {
                $participantTime = ParticipantAttendance::where([
                    ['sesi_materi.jadwal_materi_id', $workshopId],
                    ['absen_peserta.peserta_id', $participant->id]])
                ->join('sesi_materi', 'absen_peserta.sesi_materi_id', 'sesi_materi.id')
                ->sum('absen_peserta.waktu_kehadiran');

                $participant = WorkshopParticipant::where([
                    ['jadwal_materi_id', $workshopId],
                    ['peserta_id', $participant->id]
                ])->first();
                $data['waktu_kehadiran'] = $participantTime;
                $data['presentase_kehadiran'] = $participantTime / $sessionTotalDuration * 100;
                $participant->update($data);
            }
        }

        return "Update success";
    }
}