<?php

namespace App\Http\Controllers;

use App\Event;
use App\WorkshopParticipant;
use App\WorkshopSchedule;
use App\WorkshopSession;
use App\Http\Requests\WorkshopRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workshops = WorkshopSchedule::orderBy('waktu_mulai', 'asc')->get();

        return view('workshop.index', compact('workshops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($eventId)
    {
        $workshop = new WorkshopSchedule();
        $workshop['event_id'] = $eventId;
        $event = Event::findOrFail($eventId);
        $readonly = '';

        return view('workshop.create', compact('workshop', 'event', 'readonly'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkshopRequest $request)
    {
        $eventId = $request->event_id;
        $data = $request->all();
        $times = explode(" - ", $request->waktu_mulai);
        $data['waktu_mulai'] = Carbon::createFromFormat('Y/m/d H:i', $times[0]);
        $data['waktu_akhir'] = Carbon::createFromFormat('Y/m/d H:i', $times[1]);        
        $workshop = new WorkshopSchedule;
        $workshop->fill($data);

        if ($workshop->save()) {
            $session = new WorkshopSession;
            $session->jadwal_materi_id = $workshop->id;
            $session->sesi = "Sesi 1";
            $session->waktu_mulai = $workshop->waktu_mulai;
            $session->waktu_akhir = $workshop->waktu_akhir;
            // Get duration from waktu_mulai and waktu_akhir
            $startTime = Carbon::parse($session->waktu_mulai);
            $endTime = Carbon::parse($session->waktu_akhir);
            $duration = $startTime->diffInMinutes($endTime);
            $session->durasi = $duration;
            $session->save();
        }

        return redirect()->route('event.show', ['id' => $eventId]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workshop = WorkshopSchedule::findOrFail($id);
        $sessions = WorkshopSession::where('jadwal_materi_id', $id)->get();
        $participants = WorkshopParticipant::where('jadwal_materi_id', $id)
            ->join('peserta', 'peserta_materi.peserta_id', '=', 'peserta.id')
            ->select('peserta_materi.*', 'peserta.kode_peserta', 'peserta.nama', 'peserta.institusi')
            ->orderBy('nama')->get();
        // return $participants;

        return view('workshop.show', compact('workshop', 'sessions', 'participants'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workshop = WorkshopSchedule::findOrFail($id);
        $event = Event::findOrFail($workshop->event_id);
        $readonly = '';

        if (WorkshopSession::where('jadwal_materi_id', $id)->count() > 0) {
            $readonly = 'readonly';
        }

        return view('workshop.edit', compact('workshop', 'event', 'readonly'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkshopRequest $request, $id)
    {
        $workshop = WorkshopSchedule::findOrFail($id);
        $eventId = $workshop->event_id;

        $data = $request->all();
        $times = explode(" - ", $request->waktu_mulai);
        $data['waktu_mulai'] = Carbon::createFromFormat('Y/m/d H:i', $times[0]);
        $data['waktu_akhir'] = Carbon::createFromFormat('Y/m/d H:i', $times[1]);
        $workshop->update($data);

        return redirect()->route('event.show', ['id' => $eventId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workshop = WorkshopSchedule::find($id);
        $eventId = $workshop->event_id;
        $workshop->delete();

        return redirect()->route('event.show', ['id' => $eventId]);
    }

    public function schedule($filterDate = null) {
        $filterDate = ($filterDate != null) ? $filterDate : date("Y-m-d");
        $workshops = WorkshopSchedule::where('waktu_mulai', $filterDate)->orderBy('waktu_mulai')->get();

        return view('workshop.schedule', compact('workshops'));
    }
}
