<?php

namespace App\Http\Controllers;

use Auth;
use App\Participant;
use App\ParticipantAttendance;
use App\WorkshopParticipant;
use App\Http\Requests\ParticipantRequest;
use App\Http\Utilities\Utilities;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    use Utilities;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participants = Participant::orderBy('nama')->get();

        return view('participant.index', compact('participants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $participant = new Participant;

        return view('participant.create', compact('participant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ParticipantRequest $request)
    {
        $data = $request->all();
        // $data['created_by'] = Auth::user()->name;
        $participant = Participant::create($data);

        return redirect()->route('participant.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($workshopId)
    {
        $existParticipants = WorkshopParticipant::where('jadwal_materi_id', $workshopId)
            ->pluck('peserta_id')
            ->all();
        $participants = Participant::whereNotIn('id', $existParticipants)
            ->orderBy('nama')
            ->get();

        return view('participant.workshop.list', compact('workshopId', 'participants'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $participant = Participant::findOrFail($id);

        return view('participant.edit', compact('participant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ParticipantRequest $request, $id)
    {
        $participant = Participant::findOrFail($id);
        $data = $request->all();
        // $data['updated_by'] = Auth::user()->name;
        $participant->update($data);

        return redirect()->route('participant.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $participant = Participant::findOrFail($id);
        $participant->delete();

        return redirect()->route('participant.index');
    }

    /**
     * Show the form for creating a new resource in workshop page.
     *
     * @param  int  $workshopId
     * @return \Illuminate\Http\Response
     */
    public function createInWorkshop($workshopId)
    {
        $participant = new Participant;

        return view('participant.workshop.create', compact('workshopId', 'participant'));
    }

    /**
     * Create new participant in event and insert it into absen_peserta table.
     *
     * @param  int  $workshopId
     * @return \Illuminate\Http\Response
     */
    public function storeInWorkshop(ParticipantRequest $request, $workshopId)
    {
        $data = $request->all();
        $participant = new Participant;
        $participant->fill($data);

        if ($participant->save()) {
            $participantId = $participant->id;
            $workshopParticipant = new WorkshopParticipant;
            $workshopParticipant->jadwal_materi_id = $workshopId;
            $workshopParticipant->peserta_id = $participantId;

            if ($workshopParticipant->save()) {
                $this->generateAttendance($workshopId, null, $workshopParticipant->peserta_id);
            }

            return redirect()->route('workshop.show', ['id' => $workshopId]);
        }

        return redirect()->route('participant.index');
    }
}
