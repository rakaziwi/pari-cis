<?php

namespace App\Http\Controllers;

use App\Event;
use App\WorkshopParticipant;
use App\WorkshopSchedule;
use App\Http\Requests\EventRequest;
use DB;
use Excel;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::orderBy('tanggal_mulai', 'desc')->get();

        return view('event.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = new Event;
        $readonly = '';

        return view('event.create', compact('event', 'readonly'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        $data = $request->all();
        $times = explode(" - ", $request->tanggal_mulai);
        $data['tanggal_mulai'] = $times[0];
        $data['tanggal_akhir'] = $times[1];
        $event = Event::create($data);
        
        return redirect()->route('event.show', ['id' => $event->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        $workshops = WorkshopSchedule::where('event_id', $id)->orderBy('waktu_mulai')->get();

        return view('event.show', compact('event', 'workshops'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $readonly = '';

        if (WorkshopSchedule::where('event_id', $id)->count() > 0) {
            $readonly = 'readonly';
        }

        return view('event.edit', compact('event', 'readonly'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, $id)
    {
        $event = Event::findOrFail($id);
        $data = $request->all();
        $times = explode(" - ", $request->tanggal_mulai);
        $data['tanggal_mulai'] = $times[0];
        $data['tanggal_akhir'] = $times[1];
        $event->update($data);

        return redirect()->route('event.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::where('id', $id)->delete();

        return redirect()->route('event.index');
    }

    /**
     * Display a listing of participant attendances who present in event.
     *
     * @param  int  $eventId
     * @return \Illuminate\Http\Response
     */
    public function overallReport($eventId)
    {
        $event = Event::where('event.id', $eventId)
            ->join('jadwal_materi', 'jadwal_materi.event_id', 'event.id')
            ->join('sesi_materi', 'sesi_materi.jadwal_materi_id', 'jadwal_materi.id')
            ->select('event.id',
                     'event.nama',
                     'event.tempat',
                     'event.tanggal_mulai',
                     'event.tanggal_akhir',
                     DB::raw('SUM(IFNULL(sesi_materi.durasi, 0)) AS durasi'))
            ->groupBy('event.id', 'event.nama', 'event.tempat', 'event.tanggal_mulai', 'event.tanggal_akhir')
            ->first();

        $participants = WorkshopParticipant::where('jadwal_materi.event_id', $eventId)
            ->join('jadwal_materi', 'peserta_materi.jadwal_materi_id', '=', 'jadwal_materi.id')
            ->join('peserta', 'peserta_materi.peserta_id', '=', 'peserta.id')
            ->select('peserta.id',
                     'peserta.kode_peserta',
                     'peserta.nama',
                     'peserta.no_ktp',
                     'peserta.no_kta',
                     'peserta.institusi',
                     DB::raw('SUM(IFNULL(peserta_materi.waktu_kehadiran, 0)) AS waktu_kehadiran'),
                     DB::raw('TRUNCATE(AVG(IFNULL(peserta_materi.persentase_kehadiran, 0)), 2) AS persentase_kehadiran'))
            ->groupBy('peserta.id', 'peserta.kode_peserta', 'peserta.nama', 'peserta.no_ktp', 'peserta.no_kta', 'peserta.institusi')
            ->get();
        
        return view('attendance.overall-event-report', compact('event', 'participants'));
    }
    
    /**
     * Display a listing of participant attendances who present in event.
     *
     * @param  int  $eventId
     * @return \Illuminate\Http\Response
     */
    public function exportOverallReport($eventId)
    {
        $event = Event::where('event.id', $eventId)
            ->join('jadwal_materi', 'jadwal_materi.event_id', 'event.id')
            ->join('sesi_materi', 'sesi_materi.jadwal_materi_id', 'jadwal_materi.id')
            ->select('event.id',
                     'event.nama',
                     'event.tempat',
                     'event.tanggal_mulai',
                     'event.tanggal_akhir',
                     DB::raw('SUM(IFNULL(sesi_materi.durasi, 0)) AS durasi'))
            ->groupBy('event.id', 'event.nama', 'event.tempat', 'event.tanggal_mulai', 'event.tanggal_akhir')
            ->first();
        $fileName = $event->nama ." Event Attendance Report";
        $sheetName = $event->nama;
        $participants = WorkshopParticipant::where('jadwal_materi.event_id', $eventId)
            ->join('jadwal_materi', 'peserta_materi.jadwal_materi_id', '=', 'jadwal_materi.id')
            ->join('peserta', 'peserta_materi.peserta_id', '=', 'peserta.id')
            ->select('peserta.id',
                     'peserta.kode_peserta',
                     'peserta.nama',
                     'peserta.no_ktp',
                     'peserta.no_kta',
                     'peserta.institusi',
                     DB::raw('SUM(IFNULL(peserta_materi.waktu_kehadiran, 0)) AS waktu_kehadiran'),
                     DB::raw('TRUNCATE(AVG(IFNULL(peserta_materi.persentase_kehadiran, 0)), 2) AS persentase_kehadiran'))
            ->groupBy('peserta.id', 'peserta.kode_peserta', 'peserta.nama', 'peserta.no_ktp', 'peserta.no_kta', 'peserta.institusi')
            ->get();
        
        Excel::create($fileName, function($excel) use($sheetName, $event, $participants) {
            $excel->sheet($sheetName, function($sheet) use($event, $participants) {
                $sheet->loadView('attendance.export.overall-event-report')
                        ->with('event', $event)
                        ->with('participants', $participants);
            });
        })->export('xls');
    }
}
