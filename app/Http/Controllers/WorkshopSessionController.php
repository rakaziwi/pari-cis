<?php

namespace App\Http\Controllers;

use App\WorkshopParticipant;
use App\WorkshopSchedule;
use App\WorkshopSession;
use App\Http\Requests\WorkshopSessionRequest;
use App\Http\Utilities\Utilities;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WorkshopSessionController extends Controller
{
    use Utilities;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($workshopId)
    {
        $workshopSession = new WorkshopSession();
        $workshopSession['jadwal_materi_id'] = $workshopId;
        $workshop = WorkshopSchedule::findOrFail($workshopId);
        $maxSessionTime = WorkshopSession::where('jadwal_materi_id', $workshopId)->max('waktu_akhir');
        $minTime = ($maxSessionTime == '') ? $workshop->waktu_mulai->format('H:i') : (new Carbon($maxSessionTime))->format('H:i');
        $maxTime = $workshop->waktu_akhir->format('H:i');
        $startTime = $minTime;
        $endTime = $maxTime;

        return view('session.create', compact('workshopSession', 'workshop', 'minTime', 'maxTime', 'startTime', 'endTime'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkshopSessionRequest $request)
    {
        $workshopId = $request->jadwal_materi_id;
        $workshopDate = WorkshopSchedule::findOrFail($workshopId)->waktu_mulai->format('Y-m-d');
        $data = $request->all();
        $session = new WorkshopSession;
        $times = explode(" - ", $request->waktu_mulai);
        $data['waktu_mulai'] = $workshopDate .' '. $times[0] .':00';
        $data['waktu_akhir'] = $workshopDate .' '. $times[1] .':00';
        // Get duration from $times
        $startTime = Carbon::parse($times[0]);
        $endTime = Carbon::parse($times[1]);
        $duration = $startTime->diffInMinutes($endTime);
        $data['durasi'] = $duration;
        $session->fill($data);

        if ($session->save()) {
            $this->generateAttendance($workshopId, $session->id);
        }

        return redirect()->route('workshop.show', ['id' => $workshopId]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $session = WorkshopSession::findOrFail($id);
        $workshop = WorkshopSchedule::findOrFail($session->jadwal_materi_id);
        $prevSession = WorkshopSession::where([
            ['jadwal_materi_id', $session->jadwal_materi_id],
            ['waktu_akhir', '<=', $session->waktu_mulai]]
        )->count();
        $nextSession = WorkshopSession::where([
                ['jadwal_materi_id', $session->jadwal_materi_id],
                ['waktu_mulai', '>=', $session->waktu_akhir]]
            )->count();
        $minTime = ($prevSession > 0) ? $session->waktu_mulai->format('H:i') : $workshop->waktu_mulai->format('H:i');
        $maxTime = ($nextSession > 0) ? $session->waktu_akhir->format('H:i') : $workshop->waktu_akhir->format('H:i');
        $startTime = $session->waktu_mulai->format('H:i');
        $endTime = $session->waktu_akhir->format('H:i');

        return view('session.edit', compact('session', 'workshop', 'minTime', 'maxTime', 'startTime', 'endTime'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkshopSessionRequest $request, $id)
    {
        $session = WorkshopSession::findOrFail($id);
        $workshopId = $session->jadwal_materi_id;
        $workshopDate = WorkshopSchedule::findOrFail($workshopId)->waktu_mulai->format('Y-m-d');

        $data = $request->all();
        $times = explode(" - ", $request->waktu_mulai);
        $data['waktu_mulai'] = $workshopDate .' '. $times[0] .':00';
        $data['waktu_akhir'] = $workshopDate .' '. $times[1] .':00';
        // Get duration from $times
        $startTime = Carbon::parse($times[0]);
        $endTime = Carbon::parse($times[1]);
        $duration = $startTime->diffInMinutes($endTime);
        $data['durasi'] = $duration;
        $session->update($data);

        return redirect()->route('workshop.show', ['id' => $workshopId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $session = WorkshopSession::find($id);
        $workshopId = $session->jadwal_materi_id;
        $session->delete();

        return redirect()->route('workshop.show', ['id' => $workshopId]);
    }
}
