<?php

namespace App\Http\Controllers;

use App\Event;
use App\ParticipantAttendance;
use App\WorkshopParticipant;
use App\WorkshopSchedule;
use App\WorkshopSession;
use App\Http\Utilities\Utilities;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;

class ParticipantAttendanceController extends Controller
{
    use Utilities;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        DB::statement(DB::raw('set @row:=0'));
        $events = Event::selectRaw('*, @row:=@row+1 as row')
            ->orderBy('tanggal_mulai', 'desc')
            ->get();

        return view('attendance.index', compact('events'));
    }

    /**
     * Display a listing of workshop(s) in event.
     *
     * @param  int      $eventId
     * @return \Illuminate\Http\Response
     */
    public function workshop($eventId)
    {
        $event = Event::findOrFail($eventId);
        DB::statement(DB::raw('set @row:=0'));
        $workshops = WorkshopSchedule::where('event_id', $eventId)
            ->selectRaw('*, @row:=@row+1 as row')
            ->orderBy('waktu_mulai')->get();

        return view('attendance.workshop', compact('event', 'workshops'));
    }

    /**
     * Redirect to attendance workshops page when user scanning outside session period
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $eventId
     * @return \Illuminate\Http\Response
     */
    public function redirectToWorkshop(Request $request, $eventId)
    {
        // flash("Workshop: ". $request->judul_materi ." - Session: ". $request->sesi ." has been closed. Please choose another session")->error();
        \Session::flash('flash_notification_message', "Workshop: ". $request->judul_materi ." - Session: ". $request->sesi ." has been closed. Please choose another session");

        return redirect()->route('attendance.workshop', ['eventId' => $eventId]);
    }

    /**
     * Display a listing of workshop(s) in event.
     *
     * @param  bigInt   $sessionId
     * @return \Illuminate\Http\Response
     */
    public function scan($sessionId)
    {
        $session = WorkshopSession::findOrFail($sessionId);
        $workshop = WorkshopSchedule::findOrFail($session->jadwal_materi_id);
        $startTime = $session->waktu_mulai->subHours(1)->format('Y-m-d H:i');
        $endTime = $session->waktu_akhir->format('Y-m-d H:i');

        // if (($clientsLocalTime >= $startTime) && ($clientsLocalTime <= $endTime)) {
            return view('attendance.scan', compact('workshop', 'session'));
        // }

        // flash('Workshop: '. $workshop->judul_materi .' - Session: '. $session->sesi .' has been closed')->error();
        \Session::flash('flash_notification_message', 'Workshop: '. $workshop->judul_materi .' - Session: '. $session->sesi .' has been closed');
        
        return redirect()->route('attendance.workshop', ['eventId' => $workshop->event_id]);
    }

    /**
     * Display a listing of participant attendances who present in event's session of workshop.
     *
     * @param  bigInt   $sessionId
     * @return \Illuminate\Http\Response
     */
    public function report($sessionId)
    {
        $session = WorkshopSession::findOrFail($sessionId);
        $workshop = WorkshopSchedule::findOrFail($session->jadwal_materi_id);
        $participants = ParticipantAttendance::where('absen_peserta.sesi_materi_id', $sessionId)
            ->join('peserta', 'absen_peserta.peserta_id', '=', 'peserta.id')
            ->select('absen_peserta.check_in', 'absen_peserta.check_out', 'absen_peserta.waktu_kehadiran', 'peserta.*')
            ->get();

        return view('attendance.report', compact('workshop', 'session', 'participants'));
    }

    /**
     * Display a listing of participant attendances who present in event's workshop.
     *
     * @param  int  $workshopId
     * @return \Illuminate\Http\Response
     */
    public function overallReport($workshopId)
    {
        $workshop = WorkshopSchedule::findOrFail($workshopId);
        $participants = WorkshopParticipant::where('peserta_materi.jadwal_materi_id', $workshopId)
            ->join('peserta', 'peserta_materi.peserta_id', '=', 'peserta.id')
            ->select('peserta_materi.waktu_kehadiran', 'peserta_materi.persentase_kehadiran', 'peserta.*')
            ->get();

        return view('attendance.overall-report', compact('workshop', 'participants'));
    }
    
    /**
     * Display a listing of participant attendances who present in event's workshop.
     *
     * @param  int  $workshopId
     * @return \Illuminate\Http\Response
     */
    public function exportOverallReport($workshopId)
    {
        $workshop = WorkshopSchedule::findOrFail($workshopId);
        $fileName = $workshop->judul_materi ." Workshop Attendance Report";
        $sheetName = $workshop->judul_materi;
        $participants = WorkshopParticipant::where('peserta_materi.jadwal_materi_id', $workshopId)
            ->join('peserta', 'peserta_materi.peserta_id', '=', 'peserta.id')
            ->select('peserta_materi.waktu_kehadiran', 'peserta_materi.persentase_kehadiran', 'peserta.*')
            ->get();

        Excel::create($fileName, function($excel) use($sheetName, $workshop, $participants) {
            $excel->sheet($sheetName, function($sheet) use($workshop, $participants) {
                $sheet->loadView('attendance.export.overall-report')
                        ->with('workshop', $workshop)
                        ->with('participants', $participants);
            });
        })->export('xls');
    }

    /**
     * Map existing participant(s) into workshop using bulk method.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $workshopId = $request->workshop_id;
        $participants = $request->participants;
        if (WorkshopSchedule::findOrFail($workshopId) && count($participants) > 0) {
            $participantAttendances = [];
            foreach ($participants as $participant) {
                array_push($participantAttendances, [
                    'jadwal_materi_id' => $workshopId,
                    'peserta_id' => $participant,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }

            WorkshopParticipant::insert($participantAttendances);

            foreach ($participants as $participant) {
                $this->generateAttendance($workshopId, null, $participant);
            }

            \Session::flash('flash_notification_message', 'Participant(s) successful registered');
            \Session::flash('flash_notification_level', 'success');

            return redirect()->route('workshop.show', ['id' => $workshopId]);
        }

        return redirect()->route('participant.show', ['workshopId' => $workshopId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $participant = WorkshopParticipant::findOrFail($id);
        $workshopId = $participant->jadwal_materi_id;
        $participantId = $participant->peserta_id;
        if ($participant->delete()) {
            $session = WorkshopSession::where('jadwal_materi_id', $workshopId)
                ->pluck('id')
                ->all();
            $attendance = ParticipantAttendance::where('peserta_id', $participantId)
                ->whereIn('sesi_materi_id', $session)
                ->delete();
        }

        return redirect()->route('workshop.show', ['id' => $workshopId]);
    }
}
