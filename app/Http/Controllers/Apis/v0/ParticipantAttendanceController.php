<?php

namespace App\Http\Controllers\Apis\v0;

use App\Participant;
use App\ParticipantAttendance;
use App\WorkshopSchedule;
use App\WorkshopSession;
use App\Http\Controllers\Controller;
use App\Http\Utilities\Utilities;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ParticipantAttendanceController extends Controller
{
    use Utilities;

    /**
     * Scan barcode to checkin/checkout into the workshop.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scanBarCode(Request $request)
    {
        $success = false;
        $httpCode = 400;
        $workshopId = $request->workshop_id;
        $sessionId = $request->session_id;
        $barcode = $request->barcode;
        $scanTime = Carbon::parse($request->scan_time .':00')->format('Y-m-d H:i');
        $workshop = WorkshopSchedule::findOrFail($workshopId);
        // return $scanTime;

        if ($workshop) {
            $session = WorkshopSession::findOrFail($sessionId);
            
            if ($session->waktu_mulai->subHour()->format('Y-m-d H:i') <= $scanTime && $session->waktu_akhir->format('Y-m-d H:i') >= $scanTime) {
                $participant = Participant::where('barcode', $barcode)->first();

                if ($participant) {
                    // $participant = $participant->first();
                    $participantId = $participant->id;
                    $attendance = ParticipantAttendance::where([['sesi_materi_id', $sessionId], ['peserta_id', $participantId]]);
                    
                    if ($attendance->count() > 0) {
                        $attendance = $attendance->first();
                        $checkInOut = ($attendance->check_in != null) ? 'check_out' : 'check_in';
                        $attendance[$checkInOut] = $scanTime;
                        if ($checkInOut == 'check_out') {
                            $checkIn = Carbon::parse($attendance->check_in);
                            $checkOut = Carbon::parse($attendance->check_out);
                            $presentDuration = $checkIn->diffInMinutes($checkOut);
                            $attendance['waktu_kehadiran'] = $presentDuration;
                        }
                        if ($attendance->save()) {
                            if ($checkInOut == 'check_out') {
                                $this->updateAttendanceParticipantTime($workshopId, $participantId);
                            }
                        }
                        $success = true;
                        $message = "Participant ". $participant->nama ." successful ". $checkInOut;
                    } else {
                        $message = "Participant ". $participant->nama ." is not registered to this workshop, please contact your administrator";
                    }
                } else {
                    $message = "Participant with ". $barcode ." participant code is not registered, please contact your administrator";
                }
            } else {
                $message = "Workshop: ". $workshop->judul_materi ." - Session: ". $session->sesi ." has been closed. Please choose another session";
                $httpCode = 404;
            }
        } else {
            $message = "Error has been occured, please contact administrator";
        }
        
        $httpCode = ($success) ? 200 : $httpCode;

        return response()->json([
            'success' => $success,
            'message' => $message
        ], $httpCode);;
    }
}
