<?php

namespace App\Http\Controllers;

use App\ParticipantAttendance;
use App\WorkshopSchedule;
use App\WorkshopSession;
use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attendanceResumes = DB::select('SELECT
            container.nama_event,
            container.tanggal_mulai_event,
            container.tanggal_akhir_event,
            container.judul_materi,
            container.sesi,
            counter.jumlah_peserta_hadir
        FROM	(
            SELECT	e.id AS event_id,
                    e.nama AS nama_event,
                    e.tanggal_mulai AS tanggal_mulai_event,
                    e.tanggal_akhir AS tanggal_akhir_event,
                    jm.id AS jadwal_materi_id,
                    jm.judul_materi,
                    sm.id AS sesi_materi_id,
                    sm.sesi
            FROM	sesi_materi sm
                INNER JOIN jadwal_materi jm ON sm.jadwal_materi_id = jm.id
                INNER JOIN event e ON jm.event_id = e.id
            WHERE	jm.deleted_at IS NULL
                AND e.deleted_at IS NULL
        ) container
            LEFT JOIN (
                SELECT	jm.event_id,
                        jm.id AS jadwal_materi_id,
                        sm.id AS sesi_materi_id,
                        COUNT(*) AS jumlah_peserta_hadir
                FROM	absen_peserta ap
                    INNER JOIN sesi_materi sm ON ap.sesi_materi_id = sm.id
                    INNER JOIN jadwal_materi jm ON sm.jadwal_materi_id = jm.id
                WHERE	ap.check_in IS NOT NULL
                    AND	ap.check_out IS NOT NULL
                GROUP BY jm.event_id, jm.id, sm.id
            ) counter ON container.event_id = counter.event_id
                     AND container.jadwal_materi_id = counter.jadwal_materi_id
                     AND container.sesi_materi_id = counter.sesi_materi_id
            ORDER BY container.tanggal_mulai_event, container.nama_event, container.judul_materi, container.sesi ASC');

        return view('dashboard.index', compact('attendanceResumes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
