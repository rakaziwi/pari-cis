<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParticipantAttendanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sesi_materi_id' => 'required',
            'peserta_id' => 'required',
            'check_in' => 'date',
            'check_out' => 'date',
            'catatan' => 'max:255',
        ];
    }
}
