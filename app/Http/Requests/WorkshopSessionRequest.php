<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkshopSessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jadwal_materi_id' => 'required',
            'sesi' => 'required|max:255',
            // 'durasi' => 'required',
            'waktu_mulai' => 'required',
            'waktu_selesai' => 'date',
            'status' => 'max:15'
        ];
    }
}
