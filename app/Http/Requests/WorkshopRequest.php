<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkshopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_id' => 'required',
            'judul_materi' => 'required|max:255',
            'pengisi_materi' => 'max:255',
            'waktu_mulai' => 'required',
            'waktu_selesai' => 'date',
            'tempat' => 'max:255',
            'deskripsi' => 'max:255'
        ];
    }
}
