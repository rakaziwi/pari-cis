<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkshopParticipantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jadwal_materi_id' => 'required',
            'peserta_id' => 'required',
            'waktu_kehadiran' => 'number',
            'persentase_kehadiran' => 'number',
            'catatan' => 'max:255',
        ];
    }
}
