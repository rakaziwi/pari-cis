<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParticipantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_peserta' => 'required|max:20',
            'no_ktp' => 'max:30',
            'nama' => 'required|max:255',
            'no_kta' => 'max:30',
            'institusi' => 'max:100',
            'email' => 'required|max:50',
            'no_telp' => 'required|max:30',
            'jk' => 'required|max:1',
            'barcode' => 'max:255',
            'created_by' => 'max:32',
            'updated_by' => 'max:32'
        ];
    }
}
