<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipantAttendance extends Model
{
    protected $table = 'absen_peserta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sesi_materi_id',
        'peserta_id',
        'check_in',
        'check_out',
        'catatan',
        'waktu_kehadiran'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'cetak_sertifikat' => 'boolean',
    ];
}
