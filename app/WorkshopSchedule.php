<?php

namespace App;

use App\WorkshopSession;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkshopSchedule extends Model
{
    use SoftDeletes;

    protected $table = 'jadwal_materi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id',
        'judul_materi',
        'pengisi_materi',
        'waktu_mulai',
        'waktu_akhir',
        'tempat',
        'deskripsi'
    ];
    
    protected $dates = ['waktu_mulai', 'waktu_akhir', 'deleted_at'];

    public function sessions() {
        return $this->hasMany('App\WorkshopSession', 'jadwal_materi_id');
    }
}
