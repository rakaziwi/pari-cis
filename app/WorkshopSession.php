<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkshopSession extends Model
{
    protected $table = 'sesi_materi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jadwal_materi_id',
        'sesi',
        'durasi',
        'waktu_mulai',
        'waktu_akhir',
        'status'
    ];

    protected $dates = ['waktu_mulai', 'waktu_akhir'];
}
